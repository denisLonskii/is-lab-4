Лабораторная работа №4
Обработка исключений


Цель:
Научиться разделить места отлова исключений и момент выбора сообщения об ошибке.


Смысл:
Часто в приложениях выдаются неиформативные сообщения об ошибках из-за того,
что невозможно на этапе разработке предугадать. что явялется причиной
возникновения соотвествующего исключения.


Задача.
В приложении было выброшено исключение. У нас есть место вызова исключения, тип
исключения и возможно стек вызовов. Необходимо на основе этих входных данных
выдать наиболее точное сообщение о причине ошибки.

Указание. Использовать дерево решений. На одном уровне дерева будет строка
программы и файл, которые породили исключение, на следующем - тип исключения,
на еще одном - дополнительном - хэш код от стека исключений.

Предусмотреть возможность, чтобы сообщение об ошибке могло быть только одно
для конкретного типа исключения и не зависиело от места выброса, сообщение не
зависит от стека обработки.